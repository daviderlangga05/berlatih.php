<?php

class Animal
{
  public $name = "name",
    $legs = 2,
    $cold_blooded = "False";

  public function __construct($name)
  {
    $this->name = $name;
  }
}
