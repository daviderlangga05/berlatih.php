<?php
function tentukan_nilai($number)
{
  //  kode disini
  if ($number == "") {
    echo "";
  } else if ($number >= 98) {
    echo "Sangat Baik";
  } else if ($number >= 76 && $number <= 97) {
    echo "Baik";
  } else if ($number >= 67 && $number <= 77) {
    echo "Cukup";
  } elseif ($number >= 43 && $number <= 66) {
    echo "Kurang";
  } else if ($number >= 1 && $number <= 42) {
    echo "Sangat Kurang";
  }
}
function enter()
{
  echo "<br>";
}

//TEST CASES

echo tentukan_nilai(98); //Sangat Baik
echo enter();
echo tentukan_nilai(76); //Baik
echo enter();
echo tentukan_nilai(67); //Cukup
echo enter();
echo tentukan_nilai(43); //Kurang
